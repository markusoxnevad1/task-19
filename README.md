Task 19

Program for a Post Graduate Manager.

The program is connected to a university database,
consisting of students and supervisors. The program
allows for the user to add/remove students in the database.
The user can also assign students with some premade supervisors.