﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Task_19.Model;

namespace Task_19
{
    public partial class Form1 : Form
    {
        private UniversityDBContext UniversityContext;
        private int selectedSupervisorId;
        private int studentsSelectedCellKey;
        public Form1()
        {
            InitializeComponent();
            UniversityContext = new UniversityDBContext();
            List<Supervisor> supervisors = UniversityContext.Supervisors.ToList();
            foreach (Supervisor supervisor in supervisors)
            {
                // Adds all supervisors to a dropdown combobox
                comboBox1.Items.Add(supervisor.Name);
            }

            //  Connects the Supervisors datagridview to the supervisor database
            BindingSource bsSuper = new BindingSource();
            dgvSupervisors.DataSource = bsSuper;
            bsSuper.DataSource = UniversityContext.Supervisors.ToList();
            dgvSupervisors.Refresh();

            RefreshStudentDGV();
        }

        //  Creates a new student in the student database
        //  The student is asserted a supervisor by selecting one from the dropdown menu below
        private void CreateButton_Click(object sender, EventArgs e)
        {
            Supervisor newSupervisor = UniversityContext.Supervisors.Find(selectedSupervisorId);
            Student student = new Model.Student
            {
                Name = InputStudentName.Text,
                Supervisor = newSupervisor
            };
            UniversityContext.Students.Add(student);
            UniversityContext.SaveChanges();
            MessageBox.Show("Student was added");
            RefreshStudentDGV();
        }
        //  Set the index of the selected supervisor to its index + 1
        //  This is because the index in the database starts at 1, 
        //  while the index in the combox start at 0
        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSupervisorId = comboBox1.SelectedIndex+1;
        }
        //  Deleted the selected student from the student database
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            UniversityContext.Students.Remove(UniversityContext.Students.Find(this.studentsSelectedCellKey));
            UniversityContext.SaveChanges();
            MessageBox.Show("Student was removed");
            RefreshStudentDGV();
        }
        //  Retrieves the index of the selected cell
        private void DgvStudents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.studentsSelectedCellKey = int.Parse(dgvStudents.Rows[dgvStudents.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }
        //Method that refresh the student table
        private void RefreshStudentDGV()
        {
            BindingSource bsStudent = new BindingSource();
            dgvStudents.DataSource = bsStudent;
            bsStudent.DataSource = UniversityContext.Students.ToList();
            dgvStudents.Refresh();
        }

        //  Converts the Supervisor DB into a JSON string, displaying it in the JSONtextBox
        private void JSONButton_Click(object sender, EventArgs e)
        {
            List<Supervisor> supervisors = UniversityContext.Supervisors.ToList();
            string JSONString = "";
            foreach(Supervisor supervisor in supervisors)
            {
                JSONString += JsonConvert.SerializeObject(supervisor);
            }
            JSONtextBox.Text = JSONString;
        }
    }
}
