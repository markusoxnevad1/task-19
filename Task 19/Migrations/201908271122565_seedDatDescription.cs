namespace Task_19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDatDescription : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors (Name) VALUES ('Pedro')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Ryron')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Clark')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE ='Pedro'");
            Sql("DELETE FROM Supervisors WHERE ='Ryron'");
            Sql("DELETE FROM Supervisors WHERE ='Clark'");
        }
    }
}
