﻿namespace Task_19
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSupervisors = new System.Windows.Forms.DataGridView();
            this.InputStudentName = new System.Windows.Forms.TextBox();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dgvStudents = new System.Windows.Forms.DataGridView();
            this.SupervisorTableTitle = new System.Windows.Forms.Label();
            this.StudentTableTitle = new System.Windows.Forms.Label();
            this.JSONButton = new System.Windows.Forms.Button();
            this.JSONtextBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSupervisors
            // 
            this.dgvSupervisors.BackgroundColor = System.Drawing.Color.White;
            this.dgvSupervisors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupervisors.Location = new System.Drawing.Point(177, 44);
            this.dgvSupervisors.Name = "dgvSupervisors";
            this.dgvSupervisors.RowHeadersWidth = 62;
            this.dgvSupervisors.RowTemplate.Height = 28;
            this.dgvSupervisors.Size = new System.Drawing.Size(303, 196);
            this.dgvSupervisors.TabIndex = 0;
            // 
            // InputStudentName
            // 
            this.InputStudentName.Location = new System.Drawing.Point(12, 44);
            this.InputStudentName.Name = "InputStudentName";
            this.InputStudentName.Size = new System.Drawing.Size(159, 26);
            this.InputStudentName.TabIndex = 1;
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.Color.Yellow;
            this.DeleteButton.Location = new System.Drawing.Point(12, 149);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(110, 33);
            this.DeleteButton.TabIndex = 6;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.BackColor = System.Drawing.Color.Yellow;
            this.CreateButton.Location = new System.Drawing.Point(12, 110);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(110, 33);
            this.CreateButton.TabIndex = 7;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = false;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 76);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(159, 28);
            this.comboBox1.TabIndex = 9;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // dgvStudents
            // 
            this.dgvStudents.BackgroundColor = System.Drawing.Color.White;
            this.dgvStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudents.Location = new System.Drawing.Point(485, 44);
            this.dgvStudents.Name = "dgvStudents";
            this.dgvStudents.RowHeadersWidth = 62;
            this.dgvStudents.RowTemplate.Height = 28;
            this.dgvStudents.Size = new System.Drawing.Size(303, 394);
            this.dgvStudents.TabIndex = 10;
            this.dgvStudents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvStudents_CellClick);
            // 
            // SupervisorTableTitle
            // 
            this.SupervisorTableTitle.AutoSize = true;
            this.SupervisorTableTitle.BackColor = System.Drawing.Color.Silver;
            this.SupervisorTableTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.SupervisorTableTitle.Location = new System.Drawing.Point(235, 5);
            this.SupervisorTableTitle.Name = "SupervisorTableTitle";
            this.SupervisorTableTitle.Size = new System.Drawing.Size(174, 36);
            this.SupervisorTableTitle.TabIndex = 11;
            this.SupervisorTableTitle.Text = "Supervisors";
            // 
            // StudentTableTitle
            // 
            this.StudentTableTitle.AutoSize = true;
            this.StudentTableTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.StudentTableTitle.Location = new System.Drawing.Point(568, 5);
            this.StudentTableTitle.Name = "StudentTableTitle";
            this.StudentTableTitle.Size = new System.Drawing.Size(133, 36);
            this.StudentTableTitle.TabIndex = 12;
            this.StudentTableTitle.Text = "Students";
            // 
            // JSONButton
            // 
            this.JSONButton.BackColor = System.Drawing.Color.Yellow;
            this.JSONButton.Location = new System.Drawing.Point(12, 188);
            this.JSONButton.Name = "JSONButton";
            this.JSONButton.Size = new System.Drawing.Size(110, 33);
            this.JSONButton.TabIndex = 14;
            this.JSONButton.Text = "JSON";
            this.JSONButton.UseVisualStyleBackColor = false;
            this.JSONButton.Click += new System.EventHandler(this.JSONButton_Click);
            // 
            // JSONtextBox
            // 
            this.JSONtextBox.Location = new System.Drawing.Point(177, 246);
            this.JSONtextBox.Name = "JSONtextBox";
            this.JSONtextBox.Size = new System.Drawing.Size(302, 192);
            this.JSONtextBox.TabIndex = 15;
            this.JSONtextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.JSONtextBox);
            this.Controls.Add(this.JSONButton);
            this.Controls.Add(this.StudentTableTitle);
            this.Controls.Add(this.SupervisorTableTitle);
            this.Controls.Add(this.dgvStudents);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.InputStudentName);
            this.Controls.Add(this.dgvSupervisors);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSupervisors;
        private System.Windows.Forms.TextBox InputStudentName;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridView dgvStudents;
        private System.Windows.Forms.Label SupervisorTableTitle;
        private System.Windows.Forms.Label StudentTableTitle;
        private System.Windows.Forms.Button JSONButton;
        private System.Windows.Forms.RichTextBox JSONtextBox;
    }
}

