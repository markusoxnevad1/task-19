﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19.Model
{
    public class UniversityDBContext : DbContext
    {
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
